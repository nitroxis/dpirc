﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace dpbot
{
	/// <summary>
	/// Represents a queue stream with separate reading and writing positions.
	/// </summary>
	public class CircularStream : Stream
	{
		#region Fields

		private const int bufferSize = 8192;

		private readonly List<byte[]> buffers;
		private long totalLength;
		private long totalBytes;

		private int writePosition;
		private int readPosition;

		private readonly AutoResetEvent dataAvailable;

		#endregion

		#region Properties

		public override bool CanRead
		{
			get { return true; }
		}

		public override bool CanSeek
		{
			get { return false; }
		}

		public override bool CanWrite
		{
			get { return true; }
		}

		public override long Position
		{
			get
			{
				lock (this)
					return this.totalBytes;
			}
			set { throw new NotSupportedException(); }
		}

		public override long Length
		{
			get
			{
				lock (this)
					return this.totalLength;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new CircularStream.
		/// </summary>
		public CircularStream()
		{
			this.buffers = new List<byte[]>();
			this.buffers.Add(new byte[bufferSize]);
			this.dataAvailable = new AutoResetEvent(false);
		}

		#endregion

		#region Methods

		public override int Read(byte[] buffer, int offset, int count)
		{
			if (count == 0)
				return 0;

			if (this.totalLength == 0)
				this.dataAvailable.WaitOne();

			int totalBytesRead;
			lock (this)
			{
				if (count > this.totalLength)
					count = (int)this.totalLength;

				this.totalLength -= count;
				totalBytesRead = count;

				while (count > 0)
				{
					int readBytes = count;
					if (readBytes > bufferSize - this.readPosition)
						readBytes = bufferSize - this.readPosition;

					Array.Copy(this.buffers[0], this.readPosition, buffer, offset, readBytes);
					count -= readBytes;
					offset += readBytes;
					this.readPosition += readBytes;

					if (this.readPosition == bufferSize)
					{
						this.buffers.RemoveAt(0);
						this.readPosition = 0;
					}
				}

				this.totalBytes += totalBytesRead;
			}

			return totalBytesRead;
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			lock (this)
			{
				this.totalLength += count;
				while (count > 0)
				{
					int writeLength = count;
					if (writeLength > bufferSize - this.writePosition)
						writeLength = bufferSize - this.writePosition;

					Array.Copy(buffer, offset, this.buffers[this.buffers.Count - 1], this.writePosition, writeLength);
					count -= writeLength;
					offset += writeLength;
					this.writePosition += writeLength;
					if (this.writePosition == bufferSize)
					{
						this.buffers.Add(new byte[bufferSize]);
						this.writePosition = 0;
					}
				}

				if (this.totalLength > 0)
					this.dataAvailable.Set();
			}
		}

		public override void Flush()
		{
		}

		#endregion
	}
}
