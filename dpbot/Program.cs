﻿using IrcDotNet;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace dpbot
{
	public static class Program
	{
		private static readonly int[] ircPalette =
		{
			0xFFF, 0x000, 0x00F, 0x080,
			0xF00, 0x800, 0x808, 0xfa0,
			0xff0, 0x0f0, 0x088, 0x0ff,
			0x35f, 0xf0f, 0x888, 0xccc
		};

		private static readonly char[] dpConsoleChars =
		{
			'\0', '#',  '#',  '#',  '#',  '.',  '#',  '#',
			'#',  '\t', '\n', '#',  ' ',  '\r', '.',  '.',
			'[',  ']',  '0',  '1',  '2',  '3',  '4',  '5',
			'6',  '7',  '8',  '9',  '.',  '<',  '=',  '>',
			' ',  '!',  '"',  '#',  '$',  '%',  '&',  '\'',
			'(',  ')',  '*',  '+',  ',',  '-',  '.',  '/',
			'0',  '1',  '2',  '3',  '4',  '5',  '6',  '7',
			'8',  '9',  ':',  ';',  '<',  '=',  '>',  '?',
			'@',  'A',  'B',  'C',  'D',  'E',  'F',  'G',
			'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',
			'P',  'Q',  'R',  'S',  'T',  'U',  'V',  'W',
			'X',  'Y',  'Z',  '[',  '\\', ']',  '^',  '_',
			'`',  'a',  'b',  'c',  'd',  'e',  'f',  'g',
			'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',
			'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
			'x',  'y',  'z',  '{',  '|',  '}',  '~',  '<',
			'<',  '=',  '>',  '#',  '#',  '.',  '#',  '#',
			'#',  '#',  ' ',  '#',  ' ',  '>',  '.',  '.',
			'[',  ']',  '0',  '1',  '2',  '3',  '4',  '5',
			'6',  '7',  '8',  '9',  '.',  '<',  '=',  '>',
			' ',  '!',  '"',  '#',  '$',  '%',  '&',  '\'',
			'(',  ')',  '*',  '+',  ',',  '-',  '.',  '/',
			'0',  '1',  '2',  '3',  '4',  '5',  '6',  '7',
			'8',  '9',  ':',  ';',  '<',  '=',  '>',  '?',
			'@',  'A',  'B',  'C',  'D',  'E',  'F',  'G',
			'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',
			'P',  'Q',  'R',  'S',  'T',  'U',  'V',  'W',
			'X',  'Y',  'Z',  '[',  '\\', ']',  '^',  '_',
			'`',  'a',  'b',  'c',  'd',  'e',  'f',  'g',
			'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',
			'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
			'x',  'y',  'z',  '{',  '|',  '}',  '~',  '<'
		};

		private static readonly Encoding encoding = new UTF8Encoding(false, false);

		// \xFF\xFF\xFF\xFFgetchallenge
		private static readonly byte[] getChallenge = { 0xFF, 0xFF, 0xFF, 0xFF, 0x67, 0x65, 0x74, 0x63, 0x68, 0x61, 0x6C, 0x6C, 0x65, 0x6E, 0x67, 0x65 };

		private static event EventHandler<byte[]> challengeReceived;

		private enum EventType
		{
			Connected,
			Disconnected,
			GameStart,
			GameOver,
		}

		private static CircularStream logStream;
		private static StandardIrcClient irc;

		public static int Main(string[] args)
		{
			int port = 0;
			string ircHostname = "localhost";
			int ircPort = 6666;
			bool ircSsl = false;
			string ircChannel = null;
			string rconPassword = null;
			int rconSecure = 0;
			IPEndPoint rconEP = null;

			IrcUserRegistrationInfo ircInfo = new IrcUserRegistrationInfo()
			{
				NickName = "dp",
				UserName = "dpirc",
				RealName = "DarkPlaces IRC bot",
			};

			#region Command line parsing

			for (int i = 0; i < args.Length; i++)
			{
				string arg = args[i];
				bool hasValue = i < args.Length - 1;

				if (arg == "--port")
				{
					if (!hasValue)
					{
						Console.Error.WriteLine("--port needs a value.");
						return 1;
					}

					port = int.Parse(args[++i], CultureInfo.InvariantCulture);
				}
				else if (arg == "--rcon")
				{
					if (!hasValue)
					{
						Console.Error.WriteLine("--rcon needs a value.");
						return 1;
					}

					int rconPort = 26000;

					string value = args[++i];
					int colonPosition = value.LastIndexOf(':');
					if (colonPosition >= 0)
					{
						rconPort = int.Parse(value.Substring(colonPosition + 1), CultureInfo.InvariantCulture);
						value = value.Substring(0, colonPosition);
					}

					IPAddress address = IPAddress.Parse(value);
					rconEP = new IPEndPoint(address, rconPort);
				}
				else if (arg == "--rcon-password")
				{
					if (!hasValue)
					{
						Console.Error.WriteLine("--rcon-password needs a value.");
						return 1;
					}

					rconPassword = args[++i];
				}
				else if (arg == "--rcon-secure")
				{
					if (!hasValue)
					{
						Console.Error.WriteLine("--rcon-secure needs a value.");
						return 1;
					}

					rconSecure = int.Parse(args[++i], CultureInfo.InvariantCulture);
				}
				else if (arg == "--irc-server")
				{
					if (!hasValue)
					{
						Console.Error.WriteLine("--irc-server needs a value.");
						return 1;
					}

					string value = args[++i];
					int colonPosition = value.LastIndexOf(':');
					if (colonPosition >= 0)
					{
						ircPort = int.Parse(value.Substring(colonPosition + 1), CultureInfo.InvariantCulture);
						value = value.Substring(0, colonPosition);
					}

					ircHostname = value;
				}
				else if (arg == "--irc-channel")
				{
					if (!hasValue)
					{
						Console.Error.WriteLine("--irc-channel needs a value.");
						return 1;
					}

					ircChannel = args[++i];
				}
				else if (arg == "--irc-nick")
				{
					if (!hasValue)
					{
						Console.Error.WriteLine("--irc-nick needs a value.");
						return 1;
					}

					ircInfo.NickName = args[++i];
				}
				else if (arg == "--irc-username")
				{
					if (!hasValue)
					{
						Console.Error.WriteLine("--irc-username needs a value.");
						return 1;
					}

					ircInfo.UserName = args[++i];
				}
				else if (arg == "--irc-password")
				{
					if (!hasValue)
					{
						Console.Error.WriteLine("--irc-password needs a value.");
						return 1;
					}

					ircInfo.Password = args[++i];
				}
				else if (arg == "--irc-realname")
				{
					if (!hasValue)
					{
						Console.Error.WriteLine("--irc-realname needs a value.");
						return 1;
					}

					ircInfo.RealName = args[++i];
				}
				else if (arg == "--irc-ssl")
				{
					ircSsl = true;
				}
			}

			if (port <= 0 || port >= 65536)
			{
				Console.Error.WriteLine("Invalid port number.");
				return 1;
			}

			if (string.IsNullOrEmpty(ircHostname))
			{
				Console.Error.WriteLine("No IRC server specified.");
				return 1;
			}

			if (string.IsNullOrEmpty(ircChannel))
			{
				Console.Error.WriteLine("No IRC channel specified.");
				return 1;
			}

			#endregion

			bool run = true;

			#region Connect to IRC

			irc = new StandardIrcClient();
			ManualResetEvent connected = new ManualResetEvent(false);

			Console.Error.WriteLine("Connecting to IRC...");
			irc.Connect(ircHostname, ircPort, ircSsl, ircInfo);
			irc.Registered += (sender, e) =>
			{
				Console.Error.WriteLine("Connected.");
				connected.Set();
			};
			irc.ConnectFailed += (sender, e) =>
			{
				Console.Error.WriteLine($"IRC connection failed {e.Error}");
				connected.Set();
			};
			irc.Disconnected += (sender, e) =>
			{
				Console.WriteLine("Disconnected from IRC.");
				run = false;
				connected.Set();
			};

			connected.WaitOne();
			if (!irc.IsConnected)
			{
				Console.Error.WriteLine("Failed to connect to IRC.");
				return 2;
			}

			ConcurrentQueue<string> messageQueue = new ConcurrentQueue<string>();
			Thread ircSenderThread = new Thread(() => ircSender(ircChannel, messageQueue));
			ircSenderThread.Start();

			#endregion

			logStream = new CircularStream();
			UdpClient udp = new UdpClient(port);
			udp.Client.ReceiveBufferSize = 1 << 20;
			beginReceive(udp);

			EventHandler<IrcMessageEventArgs> channelMessageReceived = (sender, e) =>
			{
				IrcUser user = e.Source as IrcUser;
				if (user == null)
					return;

				string text = e.Text.Replace("\"", "\\\"");

				if (rconEP != null && rconPassword != null)
					sendRcon(udp, rconEP, rconSecure, rconPassword, $"say_as \"^7{user.NickName}\" \"{text}\"");
			};

			irc.LocalUser.JoinedChannel += (sender, e) => e.Channel.MessageReceived += channelMessageReceived;
			irc.LocalUser.LeftChannel += (sender, e) => e.Channel.MessageReceived -= channelMessageReceived;
			irc.Channels.Join(ircChannel);

			StreamReader reader = new StreamReader(logStream, encoding);

			Dictionary<EventType, Regex> regexDict = new Dictionary<EventType, Regex>();
			regexDict[EventType.Connected] = new Regex(@"^(.+) is connecting...$");
			regexDict[EventType.Disconnected] = new Regex(@"^(.+) disconnected$");
			regexDict[EventType.GameStart] = new Regex(@"^:gamestart:([^:]+):");
			regexDict[EventType.GameOver] = new Regex(@"^:gameover");

			while (run)
			{
				string line = reader.ReadLine();

				if (string.IsNullOrEmpty(line))
					continue;

				if (line.StartsWith("[::TRACE]") || line.StartsWith("[::WARNING]") || line.StartsWith("WARNING:"))
					continue;

				Console.WriteLine(line);

				// chat.
				if ((line[0] == 1 || line[0] == 3) && line.Length > 1)
				{
					messageQueue.Enqueue(dpToIrc(line.Substring(1)));
				}
				else if (line[0] == 2 && line.Length > 1)
				{
					//messageQueue.Enqueue($"\x02{dpToIrc(line.Substring(1))}");
				}
				else
				{
					// regex parsing.
					foreach (KeyValuePair<EventType, Regex> r in regexDict)
					{
						Match match = r.Value.Match(line);
						if (match.Success)
						{
							switch (r.Key)
							{
								case EventType.Connected:
									messageQueue.Enqueue(dpToIrc($"{match.Groups[1].Value} ^4connected"));
									break;

								case EventType.Disconnected:
									messageQueue.Enqueue(dpToIrc($"{match.Groups[1].Value} ^4disconnected"));
									break;

								case EventType.GameStart:
									messageQueue.Enqueue($"\x02{match.Groups[1].Value} {ircColor(4)}has begun.");
									break;

								case EventType.GameOver:
									messageQueue.Enqueue($"\x02{ircColor(4)}Game over.");
									break;
							}
							break;
						}
					}
				}
			}

			irc.Disconnect();
			ircSenderThread.Join();
			udp.Close();
			return 0;
		}

		private static void sendRcon(UdpClient udp, IPEndPoint ep, int secure, string password, string command)
		{
			if (secure == 0)
			{
				command = $"rcon {password} {command}";

				int dataLength = encoding.GetByteCount(command);
				byte[] data = new byte[dataLength + 4];
				data[0] = data[1] = data[2] = data[3] = 0xFF;
				encoding.GetBytes(command, 0, command.Length, data, 4);
				udp.Send(data, data.Length, ep);
			}
			//else if (secure == 2)
			//{
			//	udp.Send(getChallenge, getChallenge.Length, ep);
			//}
			else
			{
				throw new NotSupportedException($"rcon_secure {secure} is not supported.");
			}
		}

		private static void ircSender(string channel, ConcurrentQueue<string> messageQueue)
		{
			const int burstMax = 5;

			int burstCount = 0;

			DateTime lastMessage = DateTime.Now;
			TimeSpan floodTime = new TimeSpan(0, 0, 2);

			while (irc.IsConnected)
			{
				string message;
				if (messageQueue.TryDequeue(out message))
				{
					TimeSpan timeSinceLastMessage = DateTime.Now - lastMessage;
					if (timeSinceLastMessage > floodTime)
					{
						burstCount = 0;
					}
					else
					{
						if (++burstCount >= burstMax)
							Thread.Sleep(floodTime - timeSinceLastMessage);
					}

					if (!irc.IsConnected)
						break;

					irc.SendRawMessage($"PRIVMSG {channel} :{message}");
					lastMessage = DateTime.Now;
				}
				else
				{
					Thread.Sleep(100);
				}
			}
		}

		private static void beginReceive(UdpClient client)
		{
			client.BeginReceive(endReceive, client);
		}

		private static void endReceive(IAsyncResult result)
		{
			UdpClient client = (UdpClient)result.AsyncState;
			IPEndPoint ep = new IPEndPoint(IPAddress.Any, 0);
			byte[] data = client.EndReceive(result, ref ep);

			beginReceive(client);

			if (data.Length > 4 && BitConverter.ToInt32(data, 0) == -1)
			{
				if (data[4] == 'n')
				{
					logStream.Write(data, 5, data.Length - 5);
				}
				else if (data.Length > 14 && Encoding.ASCII.GetString(data, 4, 10) == "challenge ")
				{
					byte[] challenge = new byte[11];
					Array.Copy(data, 14, challenge, 0, 11);
					challengeReceived?.Invoke(null, challenge);
				}
			}
			else
			{
				Console.WriteLine("Unknown packet?");
				Console.WriteLine(hexDump(data));
			}
		}

		private static string hexDump(byte[] data)
		{
			StringBuilder str = new StringBuilder(data.Length * 3);

			for (int i = 0; i < data.Length; i += 16)
			{
				if (i != 0)
					str.AppendLine();

				for (int j = 0; j < 16; j++)
				{
					if (i + j < data.Length)
					{
						str.Append($"{data[i + j]:X2} ");
					}
					else
					{
						str.Append("   ");
					}
				}

				str.Append("| ");

				for (int j = 0; j < 16; j++)
				{
					if (i + j < data.Length)
					{
						if (data[i] < 32)
						{
							str.Append(".");
						}
						else
						{
							str.Append((char)data[i + j]);
						}
					}
					else
					{
						str.Append("   ");
					}
				}
			}

			return str.ToString();
		}

		private static int hexToInt(char c)
		{
			if (c >= '0' && c <= '9') return c - '0';
			if (c >= 'a' && c <= 'f') return c - 'a' + 10;
			if (c >= 'A' && c <= 'F') return c - 'A' + 10;
			return -1;
		}

		private static string ircColor(int color)
		{
			if (color < 0 || color > 15)
				throw new ArgumentOutOfRangeException(nameof(color));
			return $"\x03{color:D2}";
		}

		private static string dpToIrc(string input)
		{
			StringBuilder output = new StringBuilder(input.Length);
			for (int i = 0; i < input.Length; i++)
			{
				if ((input[i] & 0xFF00) == 0xE000)
				{
					output.Append(dpConsoleChars[input[i] & 0xFF]);
				}
				else if (input[i] == '^' && i < input.Length - 1)
				{
					char nextChar = input[i + 1];
					if (nextChar >= '0' && nextChar <= '9')
					{
						int color = nextChar - '0';
						switch (color)
						{
							case 0: output.Append(ircColor(1)); break;
							case 1: output.Append(ircColor(4)); break;
							case 2: output.Append(ircColor(9)); break;
							case 3: output.Append(ircColor(8)); break;
							case 4: output.Append(ircColor(12)); break;
							case 5: output.Append(ircColor(11)); break;
							case 6: output.Append(ircColor(13)); break;
							case 7: output.Append("\x0F"); break;
							case 8:
							case 9: output.Append(ircColor(14)); break;
						}
						i++;
					}
					else if (nextChar == 'x' && i < input.Length - 4)
					{
						int r = hexToInt(input[i + 2]);
						int g = hexToInt(input[i + 3]);
						int b = hexToInt(input[i + 4]);

						if (r >= 0 && g >= 0 && b >= 0)
						{
							int color = -1;
							int distance = int.MaxValue;

							for (int j = 0; j < ircPalette.Length; j++)
							{
								int pR = r - ((ircPalette[j] >> 8) & 0xF);
								int pG = g - ((ircPalette[j] >> 4) & 0xF);
								int pB = b - ((ircPalette[j] >> 0) & 0xF);
								int d = pR * pR + pG * pG + pB * pB;
								if (d < distance)
								{
									distance = d;
									color = j;
								}
							}

							if (color == 0) // white
								output.Append("\x0F");
							else
								output.Append(ircColor(color));

							i += 4;
						}
					}
					else
					{
						output.Append('^');
					}
				}
				else
				{
					output.Append(input[i]);
				}
			}
			return output.ToString();
		}
	}
}
